package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class MainController implements Initializable 
{

		@Override
		public void initialize(URL location, ResourceBundle resources) 
		{
			// TODO Auto-generated method stub
			
		}
		
		@FXML
		private Label lblCadastro;
		
		@FXML 
		private TextField txtNome;
		
		@FXML
		private TextField txtSobrenome;
		
		@FXML
		private TextField txtSexo;
		
		@FXML 
		private TextField txtIdade;
		
		public void Enviar (ActionEvent event) throws IOException
		{
			if (txtNome.getText().equals("Daniel") && txtSobrenome.getText().equals("Lisboa") && txtSexo.getText().equals("Masculino") && txtIdade.getText().equals("28"))
			{
				Stage primaryStage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/application/CadastroRealizado.fxml"));
				Scene scene = new Scene(root);
				primaryStage.setScene(scene);
				primaryStage.show();
			}
			
			else 
			{
				
				Stage primaryStage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/application/CadastroNaoRealizado.fxml"));
				Scene scene = new Scene(root);
				primaryStage.setScene(scene);
				primaryStage.show();
			}
		}

	}

